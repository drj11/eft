# EFT—Example Fonts for Testing

These font files have been selected because they are or have
been useful in testing various tools.
In particular `fext`, `glox`, and `glys`.

Below, where i have given a line count, it is counting the lines
in the output of `fext`.


## `ChunkFive-Regular.otf`

ChunkFive Regular, from The League of Moveable Type.
Distributed under SIL.

`fext` output:

- approx 150 lines
- Context Substitutions
- language location for Romanian (cedilla) and Turkish (i), both
  of which feature in multiple languages.


CFF notes: no subr.

The hints use some non-integer coordinates.

The **Z** shows several lines that are not quite vertical or horizontal.
Possible drawing bugs? (is this one of the things that produces
non integer hints?)


## `Compagnon-Medium.otf`

Compagnon Medium, from Velvetyne Type Foundry.

- has a feature marked as required, which upsets fext 2022-03-23
- possibly because it has no GSUB section

Some glyphs are drawn using `callgsubr`, but not `callsubr`

- E uses callgsubr
- D uses callsubr

The problem of interpreting charstrings with subrs
(we don't know how many hints there are), means that
`ttx` dumps subrs 118 and 119 as raw byte streams.
It turns out that these subrs are not used.

A brief analysis of subr usage:

    grep call font/Compagnon-Medium.ttx | awk '{print $(NF-1), $NF}' | sort -n | uniq -c

suggests that:

- both subr and gsubr are used. Equal numbers of each suggest
  that subroutines have been split in order to maximise the
  number of 1-byte literals.
- all callsubr/callgsubr are immediately preceded by a literal.
- subrs 118 119 and gsubr 120 are not called (and they are
  dumped in the ttx as `raw`, suggesting unused/corrupt?).
- a small number (two) are only called once.
- some (g)subr are used for fixed point fractions! gsubr 116 is a
  single fixed point fraction literal, used 3 times
  (for a saving of 2 bytes i think).

This font has a few more fractional coordinates.
Consider using:

    grep '\.[0-9]' font/*.ttx


## `CraftyGirls-Regular.ttf`

From
github.com/googlefonts/fonts@069dea0897b29af5e8ac3b0c7d1c7f87e0cb1280.
Distributed under Apache License.

- is a TTF (with `glyf` table)
- has no OTL tables: no GSUB, GPOS, GDEF
- is small
- also, has a `kern` table


## `GoudyBookletter1911.otf`

Goudy Bookletter 1911, from The League of Moveable Type.
Distributed under SIL.

`fext` output:

- approx 40 lines
- has UniqueID (key value 13) in CFF TopDICT, which
  opentype (and therefore `glys`) didn't parse until 2021-03-01.
- and key value 14, which it didn't parse until 2021-03-01.
- includes ligatures for U+017F LONG S

CFF notes: has subrs but they seems to be fairly naturally
arranged (compared to Ouroboros).

It has a non-standard FFNT table, which
indicates it is made with FontForge.


## `Heuristica-Regular.otf` and friends

An extended glyph set based on Adobe Utopia that was made openly
available.
A classic RIBBI family.

A well-drawn transitional (that is, a bit like Times Roman).

CFF notes: in its DICT data has the two byte operand [12 17],
Language Group, which breaks boxesandglue (2024-10-15).

OTF files from the zip available from 
https://ctan.org/tex-archive/fonts/heuristica
.


## `IBMPlexSansDevanagari-Regular.otf`

IBM Plex Sans Devanagari, from IBM.
Distributed under SIL.

- about 1200 lines (huge!)
- Charset Format 2
- Many features and lookups (for Devanagari)
- huge groups, many of which are used as before- or after- contexts


## `IBMPlexSansKR-Medium.otf`

IBM Plex Sans KR, from IBM.
Distributed under SIL.

- Induces a runtime bug in opentype Go package in 2022-03-23
- otfinfo can't parse it either, reporting:
  `otfinfo: font/IBMPlexSansKR-Medium.otf: CID-keyed fonts not supported`


## `le-murmure.otf`

Le Murmure, from Velvetyne.
Distributed under SIL.

- approx 1000 lines, but
- core ruleset of about 180 lines
- output redundantly bulked out with boiler-plate repetition
  of lookup rules for different languages
- entries in TopDict that glys 2021-12-15 can't parse
  which appears to be a bug in my opentype package
  when parsing packed decimal floats (fixed 2021-12-16)
- many stylistic alternates


## `Ouroboros-Regular.otf`

Ourobos Regular, from Velvetyne.
Distributed under SIL.

`fext` output

- 800 lines, but
- core ruleset of about 270 lines
- bulked out by multiple language boilerplate (similar to Le Murmure)
- Ligatures
- and Context Substitutions with Ligatures

CFF notes:

Appears to be heavily subroutinised.
Including subrs that set, or partially set, stem hints, and
other subrs that cannot be isolated to shapes.

A few glyphs stand out for being useful in
the development of the CFF Charstring parsing routines:

- `multiply` has no `subr` and no use of `hintmask`
- `two` does not have any `subr` in the Charstring
- both `two` and `multiply` have relatively few drawing operators.
- `heart` `eight` and `infinity` have no subr and only a
  gradually increasing use of different drawing operators.

add: parenleft u1F717 (VITRIOL) u1F71A (ALCHEMICAL SYMBOL FOR GOLD)


## `RedHatDisplay-Regular.otf`

Red Hat Diplay Regular.
This is a copy from the static OTF files of Red Hat Font.
Distributed under SIL.

- 315 lines
- fairly small and straightforward rules
- clear example of frac (and numr and dnom) feature
- clear example of case feature


## `ScreenBold.ttf` `ScreenMedium.ttf`

Screen Medium and Screen Bold are converted from the SGI Screen
fonts after they were released under MIT license.

Copied from https://web.archive.org/web/20240414193138/https://njr.sabi.net/2015/11/01/sgi-screen-fonts-converted-for-os-x/

License for distribution but it seems unlikely that Nicholas
Riley who converted the MIT licensed bitmaps would object.

- `true` magic number
- "true" bitmap
- `bhed` table; no `head`
- no `glyf` table
- `morx` table


## `ZenMaruGothic-Regular.otf`

Zen Maru Gothic Regular.
From https://github.com/googlefonts/zen-marugothic.
Distributed under OFL.

Renamed here to `.otf` with no other change.

Japanese Kanji & Hiragana & Katakana font (and also Latin).

Notable for having vertical layout as well as horizontal.
So it has a `vmtx` table.

And useful in terms on features too, not a huge number (113 lines), but
includes some reasonable small real-world examples:

- surprisingly small `frac` feature
- `fwid` and `hwid` for Latin in east asian scripts
- `vert` and `vkna` for preferred and rotated forms for vertical setting
  (and `vrt2` but in this font it is a clone of `vert`)

Also notable for having a `post` table format 2 (using the
builtin Mac Glyphs names).


## `modified-RHDR-no.otf`

A modified version of RedHatDisplay-Regular that has no OTF features.
It is made by running `makeotf` on the input file,
with no feature file specified.

- used to crash `fext` but doesn't any more.

# END
